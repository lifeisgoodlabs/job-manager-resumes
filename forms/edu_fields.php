<script type="text/html" id="tmpl-education-form">
    <div class="form-wrap">
        <div class="form-field">
            <label for="school_name_{{{data.id}}}"><?php _e('School Name', 'wp-job-resume') ?>:</label>
            <input type="text" name="school_name[]" id="school_name_{{{data.id}}}" value="{{{data.school_name}}}"/>
        </div>

        <div class="form-field">
            <label for="qualifications_{{{data.id}}}"><?php _e('Qualification(s)', 'wp-job-resume') ?>:</label>
            <input type="text" name="qualifications[]" id="qualifications_{{{data.id}}}" value="{{{data.qualifications}}}"/>
        </div>

        <div class="form-field">
            <label for="edu_start_end_{{{data.id}}}"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
            <input type="text" name="edu_start_end[]" id="edu_start_end_{{{data.id}}}" value="{{{data.start_end}}}"/>
        </div>

        <div class="form-field">
            <label for="exp_notes_{{{data.id}}}"><?php _e('Notes', 'wp-job-resume') ?>:</label>
            <textarea name="edu_notes[]" id="edu_notes_{{{data.id}}}">{{{data.notes}}}</textarea>
        </div>

        <div class="remove">
            <a href="#" class="button"><?php __('Remove','wp-job-resume') ?></a>
        </div>
        <hr>
    </div>
</script>

<div id="education_wrapper" class="form-list sortable" data-increment="<?php echo $edu_count ?>">
    <?php for($i=0;$i<$edu_count;$i++): ?>
        <div class="form-wrap">
            <div class="form-field">
                <label for="school_name_<?php echo $i ?>"><?php _e('School Name', 'wp-job-resume') ?>:</label>
                <input type="text" name="school_name[]" id="school_name_<?php echo $i ?>" value="<?php echo esc_html($school_name[$i]) ?>"/>
            </div>

            <div class="form-field">
                <label for="qualifications_<?php echo $i ?>"><?php _e('Qualification(s)', 'wp-job-resume') ?>:</label>
                <input type="text" name="qualifications[]" id="qualifications_<?php echo $i ?>" value="<?php echo esc_html($qualifications[$i]) ?>"/>
            </div>

            <div class="form-field">
                <label for="edu_start_end_<?php echo $i ?>"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
                <input type="text" name="edu_start_end[]" id="edu_start_end_<?php echo $i ?>" value="<?php echo esc_html($edu_start_end[$i]) ?>"/>
            </div>

            <div class="form-field">
                <label for="exp_notes_<?php echo $i ?>"><?php _e('Notes', 'wp-job-resume') ?>:</label>
                <textarea name="edu_notes[]" id="edu_notes_<?php echo $i ?>"><?php echo esc_textarea($edu_notes[$i]) ?></textarea>
            </div>

            <div class="remove">
                <a href="#" class="button"><?php __('Remove','wp-job-resume') ?></a>
            </div>
            <hr>
        </div>
    <?php endfor; ?>
</div>

<div class="add-form" data-source="education"><a href="#" class="button"><?php _e('Add education', 'wp-job-resume') ?></a></div>