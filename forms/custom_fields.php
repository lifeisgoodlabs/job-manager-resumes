<div class="form-wrap">
    <?php wp_nonce_field('resume_custom_fields', 'resume_wpnonce', false, true); ?>

    <div class="form-field form-required">
        <label for="first_name"><?php _e('First name', 'wp-job-resume') ?>:</label>
        <input type="text" name="first_name" id="first_name"
               value="<?php echo htmlspecialchars(get_post_meta($post->ID, 'first_name', true)) ?>"/>
    </div>

    <div class="form-field form-required">
        <label for="last_name"><?php _e('Last name', 'wp-job-resume') ?>:</label>
        <input type="text" name="last_name" id="last_name"
               value="<?php echo htmlspecialchars(get_post_meta($post->ID, 'last_name', true)) ?>"/>
    </div>

    <div class="form-field form-required">
        <label for="email"><?php _e('Contact email', 'wp-job-resume') ?>:</label>
        <input type="text" name="email" id="email"
               value="<?php echo htmlspecialchars(get_post_meta($post->ID, 'email', true)) ?>"/>
    </div>

    <div class="form-field">
        <label for="location"><?php _e('Location', 'wp-job-resume') ?>:</label>
        <input type="text" name="location" id="location"
               value="<?php echo htmlspecialchars(get_post_meta($post->ID, 'location', true)) ?>"/>
    </div>

    <div class="form-field">
        <label for="role"><?php _e('Current/Last Role', 'wp-job-resume') ?>:</label>
        <input type="text" name="role" id="role"
               value="<?php echo htmlspecialchars(get_post_meta($post->ID, 'role', true)) ?>"/>
    </div>
</div>