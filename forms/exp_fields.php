<script type="text/html" id="tmpl-experience-form">
    <div class="form-wrap">
        <div class="form-field">
            <label for="employer_{{{data.id}}}"><?php _e('Employer', 'wp-job-resume') ?>:</label>
            <input type="text" name="employer[{{{data.id}}}]" id="employer_{{{data.id}}}" value="{{{data.employer}}}"/>
        </div>

        <div class="form-field">
            <label for="job_title_{{{data.id}}}"><?php _e('Job title', 'wp-job-resume') ?>:</label>
            <input type="text" name="job_title[{{{data.id}}}]" id="job_title_{{{data.id}}}" value="{{{data.job_title}}}"/>
        </div>

        <div class="form-field">
            <label for="exp_start_end_{{{data.id}}}"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
            <input type="text" name="exp_start_end[{{{data.id}}}]" id="exp_start_end_{{{data.id}}}" value="{{{data.start_end}}}"/>
        </div>

        <div class="form-field">
            <label for="exp_notes_{{{data.id}}}"><?php _e('Notes', 'wp-job-resume') ?>:</label>
            <textarea name="exp_notes[{{{data.id}}}]" id="exp_notes_{{{data.id}}}">{{{data.notes}}}</textarea>
        </div>

        <div class="remove">
            <a href="#" class="button"><?php __('Remove','wp-job-resume') ?></a>
        </div>
    </div>
    <hr>
</script>

<div id="experience_wrapper" class="form-list sortable" data-increment="<?php echo $exp_count ?>">
    <?php for($i=0;$i<$exp_count;$i++): ?>
        <div class="form-wrap">
            <div class="form-field">
                <label for="employer_<?php echo $i ?>"><?php _e('Employer', 'wp-job-resume') ?>:</label>
                <input type="text" name="employer[<?php echo $i ?>]" id="employer_<?php echo $i ?>" value="<?php echo esc_html($employer[$i]) ?>"/>
            </div>

            <div class="form-field">
                <label for="job_title_<?php echo $i ?>"><?php _e('Job title', 'wp-job-resume') ?>:</label>
                <input type="text" name="job_title[<?php echo $i ?>]" id="job_title_<?php echo $i ?>" value="<?php echo esc_html($job_title[$i]) ?>"/>
            </div>

            <div class="form-field">
                <label for="exp_start_end_<?php echo $i ?>"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
                <input type="text" name="exp_start_end[<?php echo $i ?>]" id="exp_start_end_<?php echo $i ?>" value="<?php echo esc_html($exp_start_end[$i]) ?>"/>
            </div>

            <div class="form-field">
                <label for="exp_notes_<?php echo $i ?>"><?php _e('Notes', 'wp-job-resume') ?>:</label>
                <textarea name="exp_notes[<?php echo $i ?>]" id="exp_notes_<?php echo $i ?>"><?php echo esc_textarea($exp_notes[$i]) ?></textarea>
            </div>

            <div class="remove">
                <a href="#" class="button"><?php __('Remove','wp-job-resume') ?></a>
            </div>
        </div>
        <hr>
    <?php endfor; ?>
</div>

<div class="add-form" data-source="experience"><a href="#" class="button"><?php _e('Add experience', 'wp-job-resume') ?></a></div>
