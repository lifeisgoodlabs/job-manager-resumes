<?php
/**
 * Plugin Name: WP Job Resumes
 * Plugin URI:  http://jobmanager.lifeisgoodlabs.com/
 * Description:
 * Author:      Lifeisgoodlabs
 * Author URI:  http://lifeisgoodlabs.com
 * Version:     0.1
 * Text Domain: wp-job-resumes
 * Domain Path: /languages/
 **/

// Exit if accessed directly
if (!defined('ABSPATH')) exit;

if (!class_exists('Job_Resumes')) {
    class Job_Resumes
    {
        private $error;

        const POST_TYPE_NAME = 'resume';

        const TEMPLATES_DIR = 'templates/';
        const FORMS_DIR = 'forms/';

        /* shortcodes */
        const SHORTCODE_USER_PANEL = 'job_resume_user_panel';
        const SHORTCODE_LIST = 'job_resume_list';
            const SHORTCODE_FORM = 'job_resume_form';

        /* forms */
        const FORM_CUSTOM_FIELDS = 'custom_fields.php';
        const FORM_EDU_FIELDS = 'edu_fields.php';
        const FORM_EXP_FIELDS = 'exp_fields.php';


        /* templates */

        const TEMPLATE_RESUME_USER_PANEL = 'resume-user-panel.php';
        const TEMPLATE_RESUME_LIST = 'resume-list.php';
        const TEMPLATE_RESUME_FORM = 'resume-form.php';
        const TEMPLATE_RESUME_SINGLE = 'resume-single.php';

        /* message types */

        const TYPE_SUCCESS = 'success';
        const TYPE_ERROR = 'error';

        function __construct()
        {
            $this->error = new WP_Error();

            add_action('init',array($this,'init'));

        }

        function init () {
            $this->register_cpt();

            add_action('admin_init', array($this, 'check_required_plugins'));

            add_filter('query_vars', array($this, 'add_query_vars_filter'));

            add_action('save_post', array($this, 'save_additional_info'),10,2);

            add_action('admin_menu', array($this, 'create_custom_fields_meta_box'));
            add_action('admin_menu', array($this, 'create_education_meta_box'));
            add_action('admin_menu', array($this, 'create_experience_meta_box'));

            add_filter('manage_edit-' . self::POST_TYPE_NAME . '_columns', array($this, 'columns_head'));
            add_action('manage_' . self::POST_TYPE_NAME . '_posts_custom_column', array($this, 'columns_content'), 10, 2);

            add_shortcode(self::SHORTCODE_USER_PANEL, array($this, 'handle_shortcode_user_panel'));
            add_shortcode(self::SHORTCODE_LIST, array($this, 'handle_shortcode_resume_list'));
            add_shortcode(self::SHORTCODE_FORM, array($this, 'handle_shortcode_resume_form'));

            add_action( 'wp', array( $this, 'proccess_input_data' ) );
            add_action( 'wp', array( $this, 'remove_resume' ) );

            add_action('template_redirect',array($this,'redirect_single_resume'));

            add_action('wp_enqueue_scripts',array($this, 'enqueue_scripts'));
            add_action('admin_enqueue_scripts',array($this, 'enqueue_admin_scripts'));
        }

        public function check_required_plugins()
        {
            $is_plugin_active = is_plugin_active('wp-job-manager/wp-job-manager.php');

            if (!$is_plugin_active)
                add_action('admin_notices', array($this, 'plugin_requirements_warning'));
        }

        public function plugin_requirements_warning()
        {
            $html = '<div class="error"><p>' .
                __('Plugin <strong>"Job manager LinkedIn Application"</strong> requires a plugin:', 'wp-job-manager-applywith') .
                ' <a href="https://wordpress.org/plugins/wp-job-manager/" target="_blank">WP Job Manager</a> </p><p>' .
                __('Plugin needs to be installed and activated!', 'wp-job-manager-applywith') . '</p></div>';
            echo $html;
        }


        function add_query_vars_filter($vars)
        {
            $vars[] = "resume_id";
            $vars[] = "search";
            $vars[] = "location";
            return $vars;
        }

        protected function register_cpt () {
            $labels = array(
                'name' => _x('Resumes', 'wp-job-resume'),
                'singular_name' => _x('Resume', 'wp-job-resume'),
                'menu_name' => __('Resumes', 'wp-job-resume'),
                'parent_item_colon' => __('Parent Item:', 'wp-job-resume'),
                'all_items' => __('All Resumes', 'wp-job-resume'),
                'view_item' => __('View Resume', 'wp-job-resume'),
                'add_new_item' => __('Add New Resume', 'wp-job-resume'),
                'add_new' => __('Add New', 'wp-job-resume'),
                'edit_item' => __('Edit Resume', 'wp-job-resume'),
                'update_item' => __('Update Resume', 'wp-job-resume'),
                'search_items' => __('Search Resume', 'wp-job-resume'),
                'not_found' => __('Not found', 'wp-job-resume'),
                'not_found_in_trash' => __('Not found in Trash', 'wp-job-resume'),
            );

            $args = array(
                'label' => self::POST_TYPE_NAME,
                'description' => __('Resumes', 'wp-job-resume'),
                'labels' => $labels,
                'supports' => array('title', 'thumbnail', 'editor'),
                'hierarchical' => false,
                'public' => false,
                'show_ui' => true,
                'show_in_menu' => true,
                'show_in_nav_menus' => true,
                'show_in_admin_bar' => true,
                'menu_position' => 50,
                'can_export' => true,
                'has_archive' => true,
                'exclude_from_search' => false,
                'publicly_queryable' => true,
                'capability_type' => 'page'
            );

            register_post_type(self::POST_TYPE_NAME, $args);
        }

        public function create_custom_fields_meta_box()
        {
            add_meta_box('resume_properties', __('Resume data', 'wp-job-resume'), array($this, 'display_custom_fields'), self::POST_TYPE_NAME);
        }

        public function create_education_meta_box()
        {
            add_meta_box('resume_education_properties', __('Education', 'wp-job-resume'), array($this, 'display_education_fields'), self::POST_TYPE_NAME);
        }

        public function create_experience_meta_box()
        {
            add_meta_box('resume_experience_properties', __('Experience', 'wp-job-resume'), array($this, 'display_experience_fields'), self::POST_TYPE_NAME);
        }

        public function display_custom_fields()
        {
            global $post;

            require_once( plugin_dir_path( __FILE__ ) . self::FORMS_DIR . self::FORM_CUSTOM_FIELDS);
        }

        function display_education_fields ()
        {
            global $post;

            $school_name = unserialize(get_post_meta($post->ID, 'school_name', true));
            $qualifications = unserialize(get_post_meta($post->ID, 'qualifications', true));
            $edu_start_end = unserialize(get_post_meta($post->ID, 'edu_start_end', true));
            $edu_notes = unserialize(get_post_meta($post->ID, 'edu_notes', true));

            $edu_count = count($school_name);

            require_once( plugin_dir_path( __FILE__ ) . self::FORMS_DIR . self::FORM_EDU_FIELDS);
        }


        function display_experience_fields ()
        {
            global $post;

            $employer = unserialize(get_post_meta($post->ID, 'employer', true));
            $job_title = unserialize(get_post_meta($post->ID, 'job_title', true));
            $exp_start_end = unserialize(get_post_meta($post->ID, 'exp_start_end', true));
            $exp_notes = unserialize(get_post_meta($post->ID, 'exp_notes', true));

            $exp_count = count($employer);

            require_once( plugin_dir_path( __FILE__ ) . self::FORMS_DIR . self::FORM_EXP_FIELDS);
        }

        public function save_additional_info($post_id, $post) {

            // verify if this is an auto save routine. If it is our form has not been submitted, so we dont want to do anything
            if ( wp_is_post_revision($post) !== false ) return $post_id;

            if ( wp_is_post_autosave($post) !== false ) return $post_id;

            //don't save if only a revision
            if ( $post->post_type == 'revision' ) return $post_id;

            // Check permissions
            if ( !current_user_can( 'edit_page', $post_id ) ) return $post_id;

            if ( $post->post_type !== self::POST_TYPE_NAME ) return $post_id;

            if ( ! empty( $_POST ) && check_admin_referer( 'resume_custom_fields', 'resume_wpnonce' ) ) {
                update_post_meta( $post_id, 'first_name', esc_attr( $_POST['first_name'] ) );
                update_post_meta( $post_id, 'last_name', esc_attr( $_POST['last_name'] ) );

                $post->post_title = esc_attr($_POST['first_name'] . ' ' . $_POST['last_name']);

                update_post_meta( $post_id, 'location', esc_attr( $_POST['location'] ) );
                update_post_meta( $post_id, 'email', esc_attr( $_POST['email'] ) );
                update_post_meta( $post_id, 'role', esc_attr( $_POST['role'] ) );

                update_post_meta( $post_id, 'school_name', serialize($_POST['school_name']));
                update_post_meta( $post_id, 'qualifications', serialize($_POST['qualifications']));
                update_post_meta( $post_id, 'edu_start_end', serialize($_POST['edu_start_end']));
                update_post_meta( $post_id, 'edu_notes', serialize($_POST['edu_notes']));

                update_post_meta( $post_id, 'employer', serialize($_POST['employer']));
                update_post_meta( $post_id, 'job_title', serialize($_POST['job_title']));
                update_post_meta( $post_id, 'exp_start_end', serialize($_POST['exp_start_end']));
                update_post_meta( $post_id, 'exp_notes', serialize($_POST['exp_notes']));
            }
        }

        public function proccess_input_data() {
            if(empty($_POST)) return;

            if (!$this->check_for_login ()) return false;

            if (!isset( $_POST['resume_wpnonce'] ) || ! wp_verify_nonce( $_POST['resume_wpnonce'], 'wp_resume_form' )) {
                $this->error->add('resume_nonce', __( 'Sorry, your nonce did not verify.', 'wp-job-resume' ));
                return;
            }

            $page_id = intval($_POST['page_id']);
            $page = get_post($page_id);

            $resume_id = intval($_POST['resume_id']);
            $resume = null;
            if($resume_id) {
                $resume = get_post($_POST['resume_id']);
            }

            $data['first_name'] = sanitize_text_field($_POST['resume_first_name']);
            $data['last_name'] = sanitize_text_field($_POST['resume_last_name']);
            $data['email'] = sanitize_text_field($_POST['resume_email']);
            $data['role'] = sanitize_text_field($_POST['resume_role']);
            $data['location'] = sanitize_text_field($_POST['resume_location']);


            if($_FILES['resume_photo']) {
                $files = $this->upload_attachment('photo');
                $data['photo'] = $files[0]->{url};
            }

            $data['message'] = sanitize_text_field($_POST['resume_message']);

            $edu_error = false;
            if(is_array($_POST['school_name'])) {
                $edu_count = count($_POST['school_name']);

                $data['school_name'] = [];

                for($i=0;$i<$edu_count;$i++) {
                    $school_name = sanitize_text_field($_POST['school_name'][$i]);
                    if(empty($school_name)) {
                        $edu_error = true;
                    }

                    $data['school_name'][] = $school_name;
                    $data['qualification'][] = sanitize_text_field($_POST['qualifications'][$i]);
                    $data['edu_start_end'][] = sanitize_text_field($_POST['edu_start_end'][$i]);
                    $data['edu_notes'][] = sanitize_text_field($_POST['edu_notes'][$i]);
                }
            }

            $exp_error = false;
            if(is_array($_POST['employer'])) {
                $edu_count = count($_POST['employer']);

                $data['employer'] = [];

                for($i=0;$i<$edu_count;$i++) {
                    $employer = sanitize_text_field($_POST['employer'][$i]);
                    if(empty($employer)) {
                        $exp_error = true;
                    }

                    $data['employer'][] = $employer;
                    $data['job_title'][] = sanitize_text_field($_POST['job_title'][$i]);
                    $data['exp_start_end'][] = sanitize_text_field($_POST['exp_start_end'][$i]);
                    $data['exp_notes'][] = sanitize_text_field($_POST['exp_notes'][$i]);
                }
            }


            if($_FILES['resume_photo']) {
                try {
                    $files = $this->upload_attachment('resume_photo');
                    $data['photo'] = $files[0]->{url};

                    $image = wp_get_image_editor($files[0]->{file});
                    if(!is_wp_error($image)) {
                        $image->resize(150,150,false);
                        $image->save($files[0]->{file});
                    }
                } catch (Exception $e) {
                    $this->error->add('resume', __( 'Photo size is too big or format is invalid', 'wp-job-resume' ) );
                }
            }

            if ( empty( $page_id ) || ! $page ) {
                $this->error->add('resume', __( 'Page is not valid or unavailable', 'wp-job-resume' ) );
            }

            if ( is_object($resume) && ( $resume->post_author != get_current_user_id() || $resume->post_type != self::POST_TYPE_NAME)) {
                $this->error->add('resume', __( 'You have no access to edit this resume', 'wp-job-resume' ) );
            }

            if(empty($data['first_name'])) {
                $this->error->add('resume', __( 'Sorry, you did not to enter your first name.', 'wp-job-resume' ) );
            }

            if(empty($data['last_name'])) {
                $this->error->add('resume', __( 'Sorry, you did not to enter your last name.', 'wp-job-resume' ) );
            }

            if(!filter_var($data['email'],FILTER_VALIDATE_EMAIL)) {
                $this->error->add('resume', __( 'Sorry, you entered invalid email.', 'wp-job-resume' ) );
            }

            if($edu_error) {
                $this->error->add('resume', __( 'Sorry, you did not to enter one of the school names.', 'wp-job-resume' ) );
            }

            if($exp_error) {
                $this->error->add('resume', __( 'Sorry, you did not to enter one of the employer names.', 'wp-job-resume' ) );
            }


            if(count($this->error->get_error_messages()) == 0) {
                unset($_POST);

                if(is_object($resume)) {
                    wp_update_post(array(
                        'ID' => $resume_id,
                        'post_title' => $data['first_name'] . ' ' . $data['last_name'],
                        'post_content' => $data['message'],
                    ));
                } else {
                    $resume_id = wp_insert_post(array(
                        'post_title' => $data['first_name'] . ' ' . $data['last_name'],
                        'post_content' => $data['message'],
                        'post_type' => self::POST_TYPE_NAME,
                        'post_status' => 'publish'
                    ));
                }


                update_post_meta( $resume_id,'first_name',$data['first_name'] );
                update_post_meta( $resume_id,'last_name',$data['last_name'] );
                update_post_meta( $resume_id,'email',$data['email'] );

                update_post_meta( $resume_id, 'location', esc_attr( $data['location'] ) );
                update_post_meta( $resume_id, 'email', esc_attr( $data['email'] ) );
                update_post_meta( $resume_id, 'role', esc_attr( $data['role'] ) );
                update_post_meta( $resume_id, 'photo', $data['photo'] );

                update_post_meta( $resume_id, 'school_name', serialize($data['school_name']));
                update_post_meta( $resume_id, 'qualifications', serialize($data['qualifications']));
                update_post_meta( $resume_id, 'edu_start_end', serialize($data['edu_start_end']));
                update_post_meta( $resume_id, 'edu_notes', serialize($data['edu_notes']));

                update_post_meta( $resume_id, 'employer', serialize($data['employer']));
                update_post_meta( $resume_id, 'job_title', serialize($data['job_title']));
                update_post_meta( $resume_id, 'exp_start_end', serialize($data['exp_start_end']));
                update_post_meta( $resume_id, 'exp_notes', serialize($data['exp_notes']));


                if(is_object($resume)) {
                    $this->error->add('resume_success', __('Your resume was successfully updated', 'wp-job-resume'));
                }
                else {
                    $this->error->add('resume_success', __('Your resume was successfully added', 'wp-job-resume'));
                }
            }

        }

        public function remove_resume () {
            $resume_id = get_query_var('resume_id');

            if ($resume_id && isset($_GET['_wpnonce']) && wp_verify_nonce($_GET['_wpnonce'], 'resume_remove')) {

                if(wp_delete_post($resume_id)) {
                    $this->error->add('resume_success', __('Your resume was successfully deleted', 'wp-job-resume'));
                }
            }
        }


        public function columns_head($defaults)
        {
            $columns = array();
            $i = 0;
            foreach ($defaults as $key => $default) {
                if ($i == 2) {
                    $columns['location'] = 'Location';
                    $columns['role'] = 'Current/Last Role';
                }

                if($key == 'title')
                    $defaults[$key] = 'Candidate';

                $columns[$key] = $defaults[$key];
                $i++;
            }

            return $columns;
        }

        public function columns_content($column_name, $post_id)
        {
            if ($column_name == 'status') {
                $status = get_post_meta($post_id, 'status', true);
                echo $this->statuses[$status];
            }
            if ($column_name == 'location') {
                $location = get_post_meta($post_id, 'location', true);
                if($location)
                    echo $location;
                else
                    _e('Not specified');
            }
            if ($column_name == 'role') {
                $role = get_post_meta($post_id, 'role', true);
                if($role)
                    echo $role;
                else
                    _e('Not specified');
            }
        }

        public function redirect_single_resume () {
            global $wp;

            if(get_query_var('post_type') == self::POST_TYPE_NAME && is_singular(self::POST_TYPE_NAME))
                add_filter( 'template_include', function() {
                    return plugin_dir_path( __FILE__ ) . self::TEMPLATES_DIR . self::TEMPLATE_RESUME_SINGLE;
                });
        }



        public function handle_shortcode_user_panel () {
            if (!$this->check_for_login ()) return false;

            $args = array (
                'post_type' => self::POST_TYPE_NAME,
                'status' => 'publish',
                'author' => get_current_user_id(),
                'paged' => get_query_var('paged')
            );

            $wp_query = new WP_Query($args);
            $resumes = $wp_query->posts;

            $success = $this->error->get_error_message('resume_success');

            if($success) {
                $this->show_notice(self::TYPE_SUCCESS,$success);
            }

            require_once( plugin_dir_path( __FILE__ ) . self::TEMPLATES_DIR . self::TEMPLATE_RESUME_USER_PANEL);


            $this->pagination($wp_query->max_num_pages);
        }

        public function handle_shortcode_resume_list () {
            $search = get_query_var('search');
            $location = get_query_var('location');

            $meta_query = array ();

            if($search) {
                $meta_query[] = array(
                    'relation' => 'OR',
                    array (
                        'key' =>  'role',
                        'value' => $search,
                        'compare' => 'LIKE'
                    ),
                    array (
                        'key' => 'first_name',
                        'value' => $search,
                        'compare' => 'LIKE'
                    ),
                    array (
                        'key' => 'last_name',
                        'value' => $search,
                        'compare' => 'LIKE'
                    ),
                );
            }

            if($location) {
                $meta_query[] = array (
                    'key' => 'location',
                    'value' => $location,
                    'compare' => 'LIKE'
                );
            }

            $args = array (
                'post_type' => self::POST_TYPE_NAME,
                'status' => 'publish',
                'paged' => get_query_var('paged'),
                'meta_query' => $meta_query
            );

            $wp_query = new WP_Query($args);
            $resumes = $wp_query->posts;

            require_once( plugin_dir_path( __FILE__ ) . self::TEMPLATES_DIR . self::TEMPLATE_RESUME_LIST);

            $this->pagination($wp_query->max_num_pages);
        }

        public function handle_shortcode_resume_form () {
            global $post;

            if (!$this->check_for_login ()) return false;

            $user = wp_get_current_user();
            $data = [];

            $resume_id = get_query_var('resume_id');

            if($resume_id) {
                $resume = get_posts(array(
                    'p' => $resume_id,
                    'post_type' => self::POST_TYPE_NAME,
                    'post_author' => get_current_user_id()
                ));

                if(is_object($resume[0])) {
                    $resume = $resume[0];
                    $data = array(
                        'resume_id' => $resume->ID,
                        'resume_first_name' => get_post_meta($resume->ID, 'first_name', true),
                        'resume_last_name' => get_post_meta($resume->ID, 'last_name', true),
                        'resume_email' => get_post_meta($resume->ID, 'email', true),
                        'resume_role' => get_post_meta($resume->ID, 'role', true),
                        'resume_location' => get_post_meta($resume->ID, 'location', true),
                        'resume_message' => $resume->post_content,
                        'school_name' => unserialize(get_post_meta($resume->ID, 'school_name', true)),
                        'qualifications' => unserialize(get_post_meta($resume->ID, 'qualifications', true)),
                        'edu_start_end' => unserialize(get_post_meta($resume->ID, 'edu_start_end', true)),
                        'edu_notes' => unserialize(get_post_meta($resume->ID, 'edu_notes', true)),
                        'employer' => unserialize(get_post_meta($resume->ID, 'employer', true)),
                        'job_title' => unserialize(get_post_meta($resume->ID, 'job_title', true)),
                        'exp_start_end' => unserialize(get_post_meta($resume->ID, 'exp_start_end', true)),
                        'exp_notes' => unserialize(get_post_meta($resume->ID, 'exp_notes', true))
                    );

                    wp_reset_postdata();
                } else {
                    $this->show_notice(self::TYPE_ERROR,__('Got error loading resume','wp-job-resume'));
                }
            } else {
                $data = [
                    'resume_id' => 0
                ];
            }

            if($_POST) {
                $data = $_POST;
            }

            $success = $this->error->get_error_message('resume_success');

            if($success) {
                $this->show_notice(self::TYPE_SUCCESS,$success);
            } else {
                $errors = $this->error->get_error_messages('resume');
                $this->show_notice(self::TYPE_ERROR,$errors);
            }

            $this->back_to_user_panel_link();


            require_once( plugin_dir_path( __FILE__ ) . self::TEMPLATES_DIR . self::TEMPLATE_RESUME_FORM);
        }


        public function upload_attachment( $key ) {
            if ( isset( $_FILES[ $key ] ) && ! empty( $_FILES[ $key ] ) && ! empty( $_FILES[ $key ]['name'] ) ) {
                $files = array();
                $files_to_upload = job_manager_prepare_uploaded_files( $_FILES[ $key ] );

                foreach ( $files_to_upload as $file_to_upload ) {
                    $uploaded_file = job_manager_upload_file( $file_to_upload, array( 'file_key' => $key ) );

                    if ( is_wp_error( $uploaded_file ) ) {
                        throw new Exception( $uploaded_file->get_error_message() );
                    } else {
                        if ( ! isset( $uploaded_file->file ) ) {
                            $uploaded_file->file = str_replace( site_url(), ABSPATH, $uploaded_file->url );
                        }
                        $files[] = $uploaded_file;
                    }
                }

                return $files;
            }
        }


        protected function show_notice($type = self::TYPE_SUCCESS, $messages = '')
        {
            if(is_array($messages) && count($messages) > 0) {
                echo '<div class="'.$type.'-message">';
                foreach ($messages as $message) {
                    echo '<li>'.$message.'</li>';
                }
                echo '</div>';
            }
            elseif(is_string($messages)) {
                echo '<div class="'.$type.'-message">'.$messages.'</div>';
            }
        }

        protected function check_for_login () {
            if(!is_user_logged_in()) {
                $this->show_notice(self::TYPE_ERROR,sprintf(__('You need to <a href="%s">log in</a> to see this page','wp-job-resume'),wp_login_url()));
                return false;
            }

            return true;
        }

        protected function pagination($max_num_pages = 1)
        {
            $big = 999999999; // need an unlikely integer

            echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
                'format' => '?paged=%#%',
                'current' => max( 1, get_query_var('paged') ),
                'total' => $max_num_pages
            ) );
        }

        protected function back_to_user_panel_link () {
            echo '<p>&lt;&lt; <a href="'.job_resume_user_panel(false).'">'. __('back to user panel','wp-job-resume').'</a>';
        }


        public function enqueue_scripts () {
            wp_register_script('resume-form', plugin_dir_url( __FILE__ ) . 'assets/js/resume-form.js',array('jquery', 'wp-util'));
            wp_enqueue_script('resume-form');

            wp_register_script('resume-front', plugin_dir_url( __FILE__ ) . 'assets/js/resume-front.js',array('jquery'));
            wp_enqueue_script('resume-front');

            wp_enqueue_script('jquery-ui-sortable');

            wp_register_style('resume-form', plugin_dir_url( __FILE__ ) . 'assets/css/resume-form.css');
            wp_enqueue_style('resume-form');
        }

        public function enqueue_admin_scripts () {
            wp_register_script('resume-form', plugin_dir_url( __FILE__ ) . 'assets/js/resume-form.js',array('jquery', 'wp-util'));
            wp_enqueue_script('resume-form');

            wp_enqueue_script('jquery-ui-sortable');
        }

        /**
         * Activate the plugin
         */
        public static function activate()
        {
            // Do nothing
        }

        /**
         * Deactivate the plugin
         */
        public static function deactivate()
        {
            // Do nothing
        }
    }
}

if (class_exists('Job_Resumes')) {
    register_activation_hook(__FILE__, array('Job_Resumes', 'activate'));
    register_deactivation_hook(__FILE__, array('Job_Resumes', 'deactivate'));

    $Job_Resumes = new Job_Resumes();

    function job_resume_user_panel ($echo = true ) {
        global $wpdb;

        $query = sprintf("SELECT id FROM `%sposts` WHERE `post_content` LIKE '[%s]'",$wpdb->prefix,Job_Resumes::SHORTCODE_USER_PANEL);
        $result = $wpdb->get_results($query);

        if ($result) {
            $url = get_permalink($result[0]->id);

            if($echo)
                echo $url;
            else
                return $url;
        } else {
            return false;
        }
    }

    function job_resume_edit_link ($resume_id = 0, $echo = true ) {
        global $wpdb;

        $query = sprintf("SELECT id FROM `%sposts` WHERE `post_content` LIKE '[%s]'",$wpdb->prefix,Job_Resumes::SHORTCODE_FORM);
        $result = $wpdb->get_results($query);

        if ($result) {
            if($resume_id) {
                $url = add_query_arg(array('resume_id' => $resume_id),get_permalink($result[0]->id));
            } else {
                $url = get_permalink($result[0]->id);
            }

            if($echo)
                echo $url;
            else
                return $url;

        } else {
            return false;
        }
    }

    function job_resume_delete_link ($resume_id = 0, $echo = true )
    {
        $url = wp_nonce_url(add_query_arg(array('resume_id' => $resume_id),get_permalink()),'resume_remove');


        if($echo)
            echo $url;
        else
            return $url;
    }

    function resume_search_form()
    {
        $search = get_query_var('search');
        $location = get_query_var('location');

        ?>
        <form action="" method="GET">

            <div class="resume-search">

                <div class="search-field">
                    <label for="search"><?php _e('Keyword search','wp-job-resume') ?></label>
                    <input type="text" name="search" id="search" placeholder="Search" value="<?php echo esc_html($search) ?>">
                </div>

                <div class="location-field">
                    <label for="search_location"><?php _e('Location','wp-job-resume') ?></label>
                    <input type="text" name="location" id="search_location" placeholder="Any Location" value="<?php echo esc_html($location) ?>">
                </div>

                <div class="submit-field">
                    <input type="submit" value="Search">
                </div>
            </div>
        </form>
        <?php
    }
}