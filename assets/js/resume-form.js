jQuery(document).ready(function($){
    var dummy_data = {
        education: {
            id : 0,
            school_name : '',
            qualification : '',
            start_end : '',
            notes : ''
        },
        experience: {
            id : 0,
            employer : '',
            job_title : '',
            start_end : '',
            notes : ''
        }
    };


    $('.add-form a').click(function(e){
        e.preventDefault();
        var source = $(this).parent().data('source');
        var template = wp.template(source+'-form');

        var $wrapper = $('#'+source+'_wrapper');

        var increment = $wrapper.data('increment');


        var source_data = dummy_data[source];

        source_data.id = increment;
        $wrapper.data('increment',increment+1);

        console.log(increment);
        console.log(source_data);

        $('#'+source+'_wrapper').append(template(source_data));

    });

    $('.form-list').on('click','.remove a', function(e) {
        e.preventDefault();

        $(this).parents('.form-wrap').remove();
    });

    $( ".sortable" ).sortable();
});