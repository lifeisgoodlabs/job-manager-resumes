<form method="post" enctype="multipart/form-data" action="<?php echo esc_url( get_permalink() );?>">
    <p>
        <label><?php _e('First Name','wp-job-resume') ?> *:</label>
        <input type="text" name="resume_first_name" value="<?php echo esc_html($data['resume_first_name']) ?>">
    </p>
    <p>
        <label><?php _e('Last Name','wp-job-resume') ?> *:</label>
        <input type="text" name="resume_last_name" value="<?php echo esc_html($data['resume_last_name']) ?>">
    </p>
    <p>
        <label><?php _e('Email','wp-job-resume') ?> *:</label>
        <input type="email" name="resume_email" value="<?php echo esc_html($data['resume_email']) ?>">
    </p>
    <p>
        <label><?php _e('Professional title','wp-job-resume') ?>:</label>
        <input type="text" name="resume_role" value="<?php echo esc_html($data['resume_role']) ?>">
    </p>
    <p>
        <label><?php _e('Location','wp-job-resume') ?>:</label>
        <input type="text" name="resume_location" value="<?php echo esc_html($data['resume_location']) ?>">
    </p>
    <p>
        <label><?php _e('Photo','wp-job-resume') ?>:</label><br>
        <input type="file" name="resume_photo">
    </p>

    <p>
        <label><?php _e('Message','wp-job-resume') ?>:</label>
        <textarea name="resume_message"><?php echo esc_textarea($data['resume_message']) ?></textarea>
    </p>

    <p>Education (optional):</p>
    <script type="text/html" id="tmpl-education-form">
        <div class="form-wrap">
            <div class="form-field">
                <label for="school_name_{{{data.id}}}"><?php _e('School Name', 'wp-job-resume') ?> *:</label>
                <input type="text" name="school_name[]" id="school_name_{{{data.id}}}" value="{{{data.school_name}}}">
            </div>

            <div class="form-field">
                <label for="qualifications_{{{data.id}}}"><?php _e('Qualification(s)', 'wp-job-resume') ?>:</label>
                <input type="text" name="qualifications[]" id="qualifications_{{{data.id}}}" value="{{{data.qualifications}}}">
            </div>

            <div class="form-field">
                <label for="edu_start_end_{{{data.id}}}"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
                <input type="text" name="edu_start_end[]" id="edu_start_end_{{{data.id}}}" value="{{{data.start_end}}}">
            </div>

            <div class="form-field">
                <label for="exp_notes_{{{data.id}}}"><?php _e('Notes', 'wp-job-resume') ?>:</label>
                <textarea name="edu_notes[]" id="edu_notes_{{{data.id}}}">{{{data.notes}}}</textarea>
            </div>

            <div class="remove">
                <a href="#" class="button">remove</a>
            </div>
            <hr>
        </div>
    </script>

    <?php
        $edu_count = count($data['school_name']);
    ?>

    <div id="education_wrapper" class="form-list sortable" data-increment="<?php echo $edu_count ?>">
        <?php for($i=0;$i<$edu_count;$i++): ?>
            <div class="form-wrap">
                <div class="form-field">
                    <label for="school_name_<?php echo $i ?>"><?php _e('School Name', 'wp-job-resume') ?> *:</label>
                    <input type="text" name="school_name[]" id="school_name_<?php echo $i ?>" value="<?php echo esc_html($data['school_name'][$i]) ?>">
                </div>

                <div class="form-field">
                    <label for="qualifications_<?php echo $i ?>"><?php _e('Qualification(s)', 'wp-job-resume') ?>:</label>
                    <input type="text" name="qualifications[]" id="qualifications_<?php echo $i ?>" value="<?php echo esc_html($data['qualifications'][$i]) ?>">
                </div>

                <div class="form-field">
                    <label for="edu_start_end_<?php echo $i ?>"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
                    <input type="text" name="edu_start_end[]" id="edu_start_end_<?php echo $i ?>" value="<?php echo esc_html($data['edu_start_end'][$i]) ?>">
                </div>

                <div class="form-field">
                    <label for="exp_notes_<?php echo $i ?>"><?php _e('Notes', 'wp-job-resume') ?>:</label>
                    <textarea name="edu_notes[]" id="edu_notes_<?php echo $i ?>"><?php echo esc_textarea($data['edu_notes'][$i]) ?></textarea>
                </div>

                <div class="remove">
                    <a href="#" class="button">remove</a>
                </div>
                <hr>
            </div>
        <?php endfor; ?>
    </div>

    <div class="add-form" data-source="education"><a href="#" class="button"><?php _e('Add education', 'wp-job-resume') ?></a></div>

    <p>Experience (optional): </p>

    <script type="text/html" id="tmpl-experience-form">
        <div class="form-wrap">
            <div class="form-field">
                <label for="employer_{{{data.id}}}"><?php _e('Employer', 'wp-job-resume') ?> *:</label>
                <input type="text" name="employer[{{{data.id}}}]" id="employer_{{{data.id}}}" value="{{{data.employer}}}">
            </div>

            <div class="form-field">
                <label for="job_title_{{{data.id}}}"><?php _e('Job title', 'wp-job-resume') ?>:</label>
                <input type="text" name="job_title[{{{data.id}}}]" id="job_title_{{{data.id}}}" value="{{{data.job_title}}}">
            </div>

            <div class="form-field">
                <label for="exp_start_end_{{{data.id}}}"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
                <input type="text" name="exp_start_end[{{{data.id}}}]" id="exp_start_end_{{{data.id}}}" value="{{{data.start_end}}}">
            </div>

            <div class="form-field">
                <label for="exp_notes_{{{data.id}}}"><?php _e('Notes', 'wp-job-resume') ?>:</label>
                <textarea name="exp_notes[{{{data.id}}}]" id="exp_notes_{{{data.id}}}">{{{data.notes}}}</textarea>
            </div>

            <div class="remove">
                <a href="#" class="button">remove</a>
            </div>
        </div>
        <hr>
    </script>

    <?php
        $exp_count = count($data['employer']);
    ?>

    <div id="experience_wrapper" class="form-list sortable" data-increment="<?php echo $exp_count ?>">
        <?php for($i=0;$i<$exp_count;$i++): ?>
            <div class="form-wrap">
                <div class="form-field">
                    <label for="employer_<?php echo $i ?>"><?php _e('Employer', 'wp-job-resume') ?> *:</label>
                    <input type="text" name="employer[<?php echo $i ?>]" id="employer_<?php echo $i ?>" value="<?php echo esc_html($data['employer'][$i]) ?>">
                </div>

                <div class="form-field">
                    <label for="job_title_<?php echo $i ?>"><?php _e('Job title', 'wp-job-resume') ?>:</label>
                    <input type="text" name="job_title[<?php echo $i ?>]" id="job_title_<?php echo $i ?>" value="<?php echo esc_html($data['job_title'][$i]) ?>">
                </div>

                <div class="form-field">
                    <label for="exp_start_end_<?php echo $i ?>"><?php _e('Start / End Date', 'wp-job-resume') ?>:</label>
                    <input type="text" name="exp_start_end[<?php echo $i ?>]" id="exp_start_end_<?php echo $i ?>" value="<?php echo esc_html($data['exp_start_end'][$i]) ?>">
                </div>

                <div class="form-field">
                    <label for="exp_notes_<?php echo $i ?>"><?php _e('Notes', 'wp-job-resume') ?>:</label>
                    <textarea name="exp_notes[<?php echo $i ?>]" id="exp_notes_<?php echo $i ?>"><?php echo esc_textarea($data['exp_notes'][$i]) ?></textarea>
                </div>

                <div class="remove">
                    <a href="#" class="button">remove</a>
                </div>
            </div>
            <hr>
        <?php endfor; ?>
    </div>

    <div class="add-form" data-source="experience"><a href="#" class="button"><?php _e('Add experience', 'wp-job-resume') ?></a></div>
    <p>
        <input type="submit" value="<?php esc_attr_e( 'Submit application', 'wp-job-resume' ); ?>" />
        <input type="hidden" name="page_id" value="<?php the_ID() ?>">
        <input type="hidden" name="resume_id" value="<?php echo $data['resume_id'] ?>">
        <?php wp_nonce_field('wp_resume_form', 'resume_wpnonce', false, true); ?>
    </p>
</form>