<div class="resume-search-form">
    <?php resume_search_form() ?>
</div>

<table>
    <thead>
        <tr>
            <th><?php _e('Name / Title', 'wp-job-resume') ?></th>
            <th><?php _e('Location', 'wp-job-resume') ?></th>
            <th><?php _e('Posted', 'wp-job-resume') ?></th>
        </tr>
    </thead>
    <tbody>
    <?php if(is_array($resumes) and count($resumes)): ?>
    <?php foreach ($resumes as $resume): ?>
    <tr>
        <td>
            <a href="<?php echo get_permalink($resume->ID) ?>"><?php echo $resume->post_title ?></a><br>
            <?php echo get_post_meta($resume->ID,'role',true) ?>
        </td>
        <td><?php $location = get_post_meta($resume->ID,'location',true); echo ($location) ? $location : 'not set'; ?></td>
        <td><?php echo human_time_diff( get_post_time( 'U',false, $resume->ID ), current_time( 'timestamp' ) ); ?> <?php _e('ago', 'wp-job-resume') ?></td>
    </tr>
    <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="3"><?php _e("No active resumes available",'wp-job-resume') ?></td>
        </tr>
    <?php endif; ?>
    </tbody>
</table>


