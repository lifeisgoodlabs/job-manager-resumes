<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>

<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <?php
        // Start the loop.
        while (have_posts()) : the_post(); ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <header class="entry-header">
                    <h1 class="entry-title">
                        <?php the_title(); ?><br>
                        <small><?php echo get_post_meta(get_the_ID(), 'role', true) ?></small>
                    </h1>
                </header><!-- .entry-header -->

                <div class="post-thumbnail alignleft">
                    <img src="<?php echo get_post_meta($post->ID, 'photo', true) ?>">
                </div><!-- .post-thumbnail -->


                <div class="entry-content">
                    <?php the_content(); ?>


                    <?php
                    $school_name = unserialize(get_post_meta($post->ID, 'school_name', true));
                    $qualifications = unserialize(get_post_meta($post->ID, 'qualifications', true));
                    $edu_start_end = unserialize(get_post_meta($post->ID, 'edu_start_end', true));
                    $edu_notes = unserialize(get_post_meta($post->ID, 'edu_notes', true));

                    $edu_count = count($school_name);
                    ?>
                    <div class="entry-education">
                        <?php for($i=0;$i<$edu_count;$i++): ?>
                            <div class="education-item">
                                <h4><?php echo $school_name[$i] ?></h4>
                                <p><?php echo $edu_start_end[$i] ?></p>
                                <p><?php echo $qualifications[$i] ?></p>
                                <p><?php echo $edu_notes[$i] ?></p>
                            </div>
                            <hr>
                        <?php endfor; ?>
                    </div>

                    <?php
                    $employer = unserialize(get_post_meta($post->ID, 'employer', true));
                    $job_title = unserialize(get_post_meta($post->ID, 'job_title', true));
                    $exp_start_end = unserialize(get_post_meta($post->ID, 'exp_start_end', true));
                    $exp_notes = unserialize(get_post_meta($post->ID, 'exp_notes', true));

                    $exp_count = count($employer);
                    ?>
                    <div class="entry-experience">
                        <?php for($i=0;$i<$exp_count;$i++): ?>
                            <div class="experience-item">
                                <h4><?php echo $employer[$i] ?></h4>
                                <p><?php echo $exp_start_end[$i] ?></p>
                                <p><?php echo $job_title[$i] ?></p>
                                <p><?php echo $exp_notes[$i] ?></p>
                            </div>
                            <hr>
                        <?php endfor; ?>
                    </div>


                    <h2><?php _e("Contacts info", 'wp-job-resume') ?></h2>

                    <?php $email = get_post_meta(get_the_ID(), 'email', true); ?>
                    <div class="entry-email"><?php _e('Email', 'wp-job-resume') ?>: <a
                            href="mailto:<?php echo $email ?>"><?php echo $email ?></a></div>

                    <div class="entry-location"><?php _e('Location', 'wp-job-resume') ?>
                        : <?php echo get_post_meta(get_the_ID(), 'location', true) ?></div>

                </div><!-- .entry-content -->

            </article><!-- #post-## -->

            <?php

        endwhile;
        ?>

    </main><!-- .site-main -->

    <?php get_sidebar('content-bottom'); ?>

</div><!-- .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>
