<p><a href="<?php job_resume_edit_link() ?>"><?php _e("Add resume",'wp-job-resume') ?></a></p>

<table>
    <thead>
    <tr>
        <th><?php _e('Name / Title', 'wp-job-resume') ?></th>
        <th><?php _e('Location', 'wp-job-resume') ?></th>
        <th><?php _e('Posted', 'wp-job-resume') ?></th>
        <th></th>
    </tr>
    </thead>
    <tbody>
    <?php if(is_array($resumes) and count($resumes)): ?>
    <?php foreach ($resumes as $resume): ?>
        <tr>
            <td>
                <a href="<?php the_permalink($resume->ID) ?>"><?php echo $resume->post_title ?></a><br>
                <?php echo get_post_meta($resume->ID,'role',true) ?>
            </td>
            <td><?php $location = get_post_meta($resume->ID,'location',true); echo ($location) ? $location : 'not set'; ?></td>
            <td><?php echo human_time_diff( get_post_time( 'U',false, $resume->ID ), current_time( 'timestamp' ) ); ?> <?php _e('ago', 'wp-job-resume') ?></td>
            <td>
                <a href="<?php echo job_resume_edit_link($resume->ID) ?>" data-action="edit"><?php _e('Edit','wp-job-resume') ?></a>
                <a href="<?php echo job_resume_delete_link($resume->ID) ?>" data-action="delete"><?php _e('Delete','wp-job-resume') ?></a>
            </td>
        </tr>
    <?php endforeach; ?>
    <?php else: ?>
        <tr>
            <td colspan="4"><?php _e("You have no resumes",'wp-job-resume') ?></td>
        </tr>
    <?php endif; ?>
    </tbody>

</table>

